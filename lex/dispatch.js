'use strict';

const aboutMe = require('./intents/about-me');
const greetingUser = require('./intents/greeting-user');

module.exports = function(intentRequest) {
    console.log(`dispatch userId=${intentRequest.userId}, intentName=${intentRequest.currentIntent.name}`);
    const intentName = intentRequest.currentIntent.name;

    if (intentName === 'AboutMe') {
        console.log(`${intentName} is called`);
        return aboutMe(intentRequest);
    }

    if (intentName === 'GreetingUser') {
        console.log(`${intentName} is called`);
        return greetingUser(intentRequest);
    }

    throw new Error(`Intent with a name ${intentName} is not supported`);
};
