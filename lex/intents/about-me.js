'use strict';

const builder = require('../builder');


function validateAnswer(answer) {
    console.log(`answer: ${answer}`);
    // if (username && profanity.check(username).length > 0) {
    //     return builder.validationBuilder(false, 'username', `I don't think ${username} is your name. Tell me your real name.`, null);
    // }
    return builder.validationBuilder(true, null, null, null);
}


module.exports = function(intentRequest) {
    console.log(intentRequest.currentIntent.slots);
    const answer = intentRequest.currentIntent.slots['answer'];
    const source = intentRequest.invocationSource;
    console.log(`source: ${source}`);

    if (source === 'DialogCodeHook') {
        const slots = intentRequest.currentIntent.slots;
        const validationResult = validateAnswer(answer);

        if (!validationResult.isValid) {
            slots[`${validationResult.violatedSlot}`] = null;
            if (validationResult.options) {
                return Promise.resolve(builder.elicitSlot(
                    intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots,
                    validationResult.violatedSlot, validationResult.message,
                    validationResult.options.title, validationResult.options.imageUrl, validationResult.options.buttons
                ));
            } else {
                return Promise.resolve(builder.elicitSlot(
                    intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots,
                    validationResult.violatedSlot, validationResult.message,
                    null, null, null
                ));
            }
        }
        return Promise.resolve(builder.delegate(intentRequest.sessionAttributes, intentRequest.currentIntent.slots));

    } else if (source === 'FulfillmentCodeHook') {
        if (answer === 'false') {
            return Promise.resolve(builder.close(
                intentRequest.sessionAttributes, 'Fulfilled',
                builder.messageBuilder('Okay, maybe later.', { answer: false })
            ));
        } else {
            return Promise.resolve(builder.close(
                intentRequest.sessionAttributes, 'Fulfilled',
                builder.messageBuilder('Thank you for asking! My name is captainwhale.', { answer: true, botname: 'captainwhale' })
            ));
        }
    }
};
