// 'use strict';
//
// const lexResponses = require('../responses');
// const validationBuilder = require('../builder');
//
//
// function getButtons(options) {
//     const buttons = [];
//     options.forEach(option => {
//        buttons.push({
//             text: option,
//             value: option
//         });
//     });
//     return buttons;
// }
//
// function getOptions(title, types) {
//     return {
//         title,
//         imageUrl: null,
//         buttons: getButtons(types)
//     };
// }
//
// function validateFlowerOrder(flowerType) {
//     if (flowerType && !flowerType.toLowerCase().includes('tulip')) {
//         // const options = getOptions('Select a coffee', types);
//         return validationBuilder(false, 'FlowerType', `We do not have ${flowerType}.`, null);
//     }
//     return validationBuilder(true, null, null, null);
// }
//
// module.exports = function(intentRequest) {
//     console.log(intentRequest.currentIntent.slots);
//     const flowerType = intentRequest.currentIntent.slots['FlowerType'];
//     const pickupDate = intentRequest.currentIntent.slots['PickupDate'];
//     const pickupTime = intentRequest.currentIntent.slots['PickupTime'];
//
//     console.log(`${flowerType} ${pickupDate} ${pickupTime}`);
//
//     const source = intentRequest.invocationSource;
//
//     if (source === 'DialogCodeHook') {
//         const slots = intentRequest.currentIntent.slots;
//         const validationResult = validateFlowerOrder(flowerType);
//
//         if (!validationResult.isValid) {
//             slots[`${validationResult.violatedSlot}`] = null;
//             if (validationResult.options) {
//                 return Promise.resolve(lexResponses.elicitSlot(
//                     intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots,
//                     validationResult.violatedSlot, validationResult.message,
//                     validationResult.options.title, validationResult.options.imageUrl, validationResult.options.buttons
//                 ));
//             } else {
//                 return Promise.resolve(lexResponses.elicitSlot(
//                     intentRequest.sessionAttributes, intentRequest.currentIntent.name, slots,
//                     validationResult.violatedSlot, validationResult.message,
//                     null, null, null
//                 ));
//             }
//
//         }
//
//         return Promise.resolve(lexResponses.delegate(intentRequest.sessionAttributes, intentRequest.currentIntent.slots));
//     }
// };
