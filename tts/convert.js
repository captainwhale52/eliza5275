'use strict';

const AWS = require('aws-sdk');
const s3 = new AWS.S3({apiVersion: process.env.AWS_API_VERSION});
const textToSpeech = require('./text-to-speech');

const striptags = require('striptags');
const path = require('path');
const Entities = require('html-entities').XmlEntities;

const entities = new Entities();


/**
 * Converts blog text to mp3 and saves to S3 bucket
 * @param event
 * @returns {Promise.<string>}
 */
module.exports = (event) => {
    let name = 'stranger';
    if (event.queryStringParameters && event.queryStringParameters.name) {
        name = event.queryStringParameters.name;
    }

    return textToSpeech(`Hello, ${name}`)
        .then(data => s3.putObject({
            Bucket: process.env.POLLY_BUCKET,
            Key: `${name}.mp3`,
            Body: data.AudioStream,
            ContentType: 'audio/mpeg',
        }).promise())
        .then(data => {
            const url = s3.getSignedUrl('getObject', {
                Bucket: process.env.POLLY_BUCKET,
                Key: `${name}.mp3`,
                Expires: 15
            });

            return {
                statusCode: 200,
                headers: {
                    'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                },
                body: JSON.stringify({
                    audio_url: `${url}`,
                    message: `Hello, ${name}`
                }),
            };
        });

    // const s3data = event.Records[0].s3;
    // const id = path.basename(s3data.object.key, '.json');
    //
    // let json;
    //
    // return s3.getObject({
    //     Bucket: s3data.bucket.name,
    //     Key: s3data.object.key,
    // }).promise()
    // .then((data) => {
    //     json = JSON.parse(data.Body);
    //     const text = entities.decode(striptags(json.description));
    //     return textToSpeech(`${json.title}. ${text}`);
    // })
    // .then(data => s3.putObject({
    //     Bucket: process.env.POLLY_BUCKET,
    //     Key: `${id}.mp3`,
    //     Body: data.AudioStream,
    //     ContentType: 'audio/mpeg',
    // }).promise())
    // .then(() => `${id}.mp3 created`);
};
