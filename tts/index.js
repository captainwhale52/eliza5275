'use strict';

const convert = require('./convert');

// module.exports.handler =
//   (event, context, callback) =>
//     convert(event)
//       .then(data => callback(null, data))
//       .catch(callback);



module.exports.handler = (event, context, callback) => {
    convert(event)
        .then(data => callback(null, data))
        .catch(callback);
    // let name = 'stranger';
    // if (event.queryStringParameters && event.queryStringParameters.name) {
    //     name = event.queryStringParameters.name;
    // }
    //
    // const response = {
    //     statusCode: 200,
    //     headers: {
    //         'Access-Control-Allow-Origin': '*', // Required for CORS support to work
    //     },
    //     body: JSON.stringify({
    //         message: `Hello, ${name}!`,
    //     }),
    // };

    // callback(null, response);

    // convert(event)
    //     .then(data => callback(null, data))
    //     .catch(callback);
    // const response = {
    //     statusCode: 200,
    //     headers: {
    //         'Access-Control-Allow-Origin': '*', // Required for CORS support to work
    //     },
    //     body: JSON.stringify({
    //         message: 'Go Serverless v1.0! Your function executed successfully!',
    //         input: event,
    //     }),
    // };
    //
    // callback(null, response);
};