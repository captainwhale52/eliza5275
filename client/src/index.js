import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';

import Typed from 'typed.js';
// import {
//     BrowserRouter as Router,
//     Route,
//     Link
// } from 'react-router-dom';

import store from './events/stores/store-index';


import Screen from './components/screen';
// import About from './components/about';
// import Home from './components/home';
// import Header from './components/header/header-index';

import styles from './styles/index.scss';


class App extends React.Component {
    getChildContext() {
        return { styles };
    }
    render() {
        return (
            <Provider store={store}>
                <Screen />
            </Provider>
        );
    }
}

App.childContextTypes = {
    styles: PropTypes.object,
};

ReactDOM.render(<App />, document.getElementById("app"));