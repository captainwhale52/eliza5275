import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Typed from "typed.js";

import GLOBALS from './../globals/global-index';

import bootupScreenLaptop from '../media/bootup-screen-laptop.txt';
import bootupScreenMobile from '../media/bootup-screen-mobile.txt';
import bootupScreenTest from '../media/bootup-screen-test.txt';

import { setIsTyping, sendMessage } from '../events/actions/action-app';

class Component extends React.Component {
    constructor(props) {
        super(props);
        this.bootupScreen = React.createRef();
    }

    componentWillMount() {
    }

    componentDidMount() {
        let bootupScreen = bootupScreenLaptop;
        if (window.innerWidth < 1024) {
            bootupScreen = bootupScreenMobile;
        }
        new Typed(this.bootupScreen.current, {
            strings: [bootupScreen.replace(/\\n/g , '<br/>')],
            smartBackspace: true,
            startDelay: 0,
            typeSpeed: 25,
            shuffle: false,
            showCursor: false,
            onComplete: (self) => {
                // this.props.setIsTyping(false);
                this.props.sendMessage('hello');
            },
            preStringTyped: (arrayPos, self) => {
                this.props.setIsTyping(true);
            },
        });
    }

    render() {
        return (
            <section>
                <p ref={this.bootupScreen} />
            </section>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        // slide: state.slide,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setIsTyping,
        sendMessage
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);


