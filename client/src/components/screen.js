import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import GLOBALS from './../globals/global-index';

import { setIsTyping, consumeNewMessage, sendMessage } from "../events/actions/action-app";
import Bootup from './bootup';
import Typed from "typed.js";


class Component extends React.Component {
    constructor(props) {
        super(props);
        this.mainContainer = React.createRef();
        this.state = {
            messageHistory: [],
            currentMessage: '',
            historyIndex: 0,
            lastMessageTimestamp: new Date().valueOf()
        };
        this.newMessageContainer = React.createRef();
    }

    componentWillMount() {
    }

    componentDidMount() {
        window.addEventListener("keydown", (event) => {
            if (this.props.app.isTyping) {
                return;
            }
            if (event.defaultPrevented) {
                return; // Do nothing if the event was already processed
            }
            if (event.key === "A" || event.key === "B" || event.key === "C" || event.key === "D" || event.key === "E" || event.key === "F" || event.key === "G" || event.key === "H" || event.key === "I" || event.key === "J" || event.key === "K" || event.key === "L" || event.key === "M" || event.key === "N" || event.key === "O" || event.key === "P" || event.key === "Q" || event.key === "R" || event.key === "S" || event.key === "T" || event.key === "U" || event.key === "V" || event.key === "W" || event.key === "X" || event.key === "Y" || event.key === "Z" || event.key === "a" || event.key === "b" || event.key === "c" || event.key === "d" || event.key === "e" || event.key === "f" || event.key === "g" || event.key === "h" || event.key === "i" || event.key === "j" || event.key === "k" || event.key === "l" || event.key === "m" || event.key === "n" || event.key === "o" || event.key === "p" || event.key === "q" || event.key === "r" || event.key === "s" || event.key === "t" || event.key === "u" || event.key === "v" || event.key === "w" || event.key === "x" || event.key === "y" || event.key === "z" || event.key === "1" || event.key === "2" || event.key === "3" || event.key === "4" || event.key === "5" || event.key === "6" || event.key === "7" || event.key === "8" || event.key === "9" || event.key === "0" || event.key === ":" || event.key === "/" || event.key === "~" || event.key === "!" || event.key === "@" || event.key === "#" || event.key === "$" || event.key === "%" || event.key === "&" || event.key === "*" || event.key === "(" || event.key === ")" || event.key === "-" || event.key === "+" || event.key === "_" || event.key === "=" || event.key === " " || event.key === "?" || event.key === "," || event.key === "." || event.key === "\\" || event.key === "{" || event.key === "}" || event.key === "[" || event.key === "]" || event.key === "|" || event.key === "^" || event.key === "`" || event.key === "'" || event.key === "\"" || event.key === ":" || event.key === ";" || event.key === "<" || event.key === ">" || event.key === "/") {
                this.setState({
                    currentMessage: this.state.currentMessage + event.key
                });
                event.preventDefault();
            } else if (event.key === 'Backspace') {
                this.setState({
                    currentMessage: this.state.currentMessage.substr(0, Math.max(0, this.state.currentMessage.length - 1))
                });
                event.preventDefault();
            } else if (event.key === 'Enter' && this.state.currentMessage.trim() !== '') {
                this.props.sendMessage(this.state.currentMessage.trim());
                const messageHistory = this.state.messageHistory;
                messageHistory.push(`${this.props.app.username}> ${this.state.currentMessage}`);
                this.setState({
                    currentMessage: '',
                    messageHistory,
                    historyIndex: messageHistory.length - 1,
                });
                // setTimeout(() => {
                //     this.mainContainer.current.scrollTop = this.mainContainer.current.scrollHeight;
                // }, 50);
                event.preventDefault();
            }
            // TODO-Karl: Filter out the bot messages.
            // else if (event.key === 'ArrowUp') {
            //     let lastMessage = '';
            //     let i = Math.min(this.state.historyIndex, this.state.messageHistory.length - 1);
            //     for (; i >= 0 ; i--) {
            //         lastMessage = this.state.messageHistory[i].substr(this.state.messageHistory[i].indexOf('>') + 1, this.state.messageHistory[i].length - 1).trim();
            //         if (lastMessage !== '') {
            //             break;
            //         }
            //     }
            //     this.setState({
            //         currentMessage: lastMessage,
            //         historyIndex: i - 1,
            //     });
            //     event.preventDefault();
            // } else if (event.key === 'ArrowDown') {
            //     let lastMessage = '';
            //     let i = Math.max(this.state.historyIndex, 0);
            //     for (; i < this.state.messageHistory.length - 1 ; i++) {
            //         lastMessage = this.state.messageHistory[i].substr(this.state.messageHistory[i].indexOf('>') + 1, this.state.messageHistory[i].length - 1).trim();
            //         if (lastMessage !== '') {
            //             break;
            //         }
            //     }
            //     this.setState({
            //         currentMessage: lastMessage,
            //         historyIndex: i + 1,
            //     });
            //     event.preventDefault();
            // }

            setTimeout(() => {
                this.mainContainer.current.scrollTop = this.mainContainer.current.scrollHeight;
            }, 100);

            // Cancel the default action to avoid it being handled twice
            // event.preventDefault();
        }, true);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.app.isTyping) {
            if (this.scrollToBottom) {
                clearInterval(this.scrollToBottom);
            }
            this.scrollToBottom = setInterval(() => {
                this.mainContainer.current.scrollTop = this.mainContainer.current.scrollHeight;
            }, 100);
            // Retrieve a new message.
            console.log(nextProps.app.newMessage);
            if (nextProps.app.newMessage !== null) {
                if (this.state.lastMessageTimestamp !== nextProps.app.newMessage.timestamp) {
                    this.setState({
                        lastMessageTimestamp: nextProps.app.newMessage.timestamp
                    });
                    this.newMessageContainer.current.innerHTML = '';
                    new Typed(this.newMessageContainer.current, {
                        strings: [`${this.props.app.botname}> ${nextProps.app.newMessage.message}`],
                        smartBackspace: true,
                        startDelay: 0,
                        typeSpeed: 25,
                        shuffle: false,
                        showCursor: false,
                        onComplete: (self) => {
                            this.newMessageContainer.current.innerHTML = '';

                            const messageHistory = this.state.messageHistory;
                            messageHistory.push(`${this.props.app.botname}> ${nextProps.app.newMessage.message}`);
                            this.setState({
                                messageHistory,
                            });

                            this.props.setIsTyping(false);
                        },
                        preStringTyped: (arrayPos, self) => {
                            this.props.setIsTyping(true);
                        },
                    });
                    this.props.consumeNewMessage();
                }
            }
        } else {
            if (this.scrollToBottom) {
                clearInterval(this.scrollToBottom);
            }
            // this.mainContainer.scrollTop = this.mainContainer.scrollHeight;
        }
    }

    render() {
        const typingClass = this.props.app.isTyping ? this.context.styles['typing'] : '';

        const pastMessages = this.state.messageHistory.map((message, index) => {
            return <p key={`message-${index}`}><span>{message}</span></p>
        });
        return (
            <main ref={this.mainContainer} className={typingClass}>
                <Bootup />
                <div>
                    { pastMessages }
                </div>
                <p ref={this.newMessageContainer} />
                <div className={this.context.styles['message']}>
                    <span>{`${this.props.app.username}> `}</span>
                    <span>{this.state.currentMessage}</span>
                    <span className={this.context.styles['cursor']}> </span>
                </div>
            </main>
        );
    }
}

Component.contextTypes = {
    styles: PropTypes.object
};

function mapStateToProps(state) {
    return {
        app: state.app,
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setIsTyping, consumeNewMessage, sendMessage
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
