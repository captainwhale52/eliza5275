import api from './api';

import GLOBALS from './../../globals/global-index';


const setIsTyping = isTyping => (dispatch) => {
    dispatch({
        type: 'SET_IS_TYPING',
        payload: {
            isTyping
        }
    });
    return Promise.resolve(
    ).then(success => success);
};


const setUsername = username => (dispatch) => {
    dispatch({
        type: 'SET_USERNAME',
        payload: {
            username
        }
    });
    return Promise.resolve(
    ).then(success => success);
};


const sendMessage = (message) => (dispatch) => {
    console.log(message);
    dispatch({
        type: 'SEND_MESSAGE_PENDING',
    });

    const config = {};
    // const config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } };
    // const data = new FormData();
    // data.append('image', image);

    return api.post('chat/', {
        message: message
    }, config).then((success) => {
        console.log(success);
        dispatch({
            type: 'SEND_MESSAGE_FULFILLED',
            payload: success.data
        });
    }).catch((error) => {
        dispatch({
            type: 'SEND_MESSAGE_REJECTED',
            payload: error,
        });
    });
};

const consumeNewMessage = (message) => (dispatch) => {
    dispatch({
        type: 'CONSUME_NEW_MESSAGE',
        payload: {
            message
        }
    });
    return Promise.resolve(
    ).then(success => success);
};


export {
    setIsTyping, setUsername, sendMessage, consumeNewMessage
};

