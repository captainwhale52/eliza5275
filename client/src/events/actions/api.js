import axios from 'axios';

import GLOBALS from '../../globals/global-index';

const uniqid = require('uniqid');

// import cookie from 'js-cookie';

const api = axios.create({
    baseURL: GLOBALS.API.API_BASE,
});

const uniqueUserId = uniqid();

// console.log(cookie.get('authentication'));
// Add a request interceptor.
api.interceptors.request.use((config) => {
    config.headers['Content-Type'] = 'application/json';
    config.headers['Authorization'] = uniqueUserId;
    // config.headers['Authentication'] = cookie.get('authentication'); // Add a JWT if there is any necessity.
    // if (cookie.get('authentication')) {
    //     config.headers['Authorization'] = `JWT ${cookie.get('authentication')}`; // Add a JWT if there is any necessity.
    // }
    // config.headers['X-CSRFToken'] = cookie.get('csrftoken');    // CSRF Token
    return config;
}, error => Promise.reject(error));

export default api;
