// import Immutable from 'seamless-immutable';

import GLOBALS from '../../globals/global-index';
import Immutable from "seamless-immutable";

const defaultState = Immutable({
    author: GLOBALS.APP.AUTHOR,
    isTyping: false,
    username: 'you',
    botname: 'robot',
    newMessage: null,
    // components: [],
    // mode: GLOBALS.SLIDE.MODE.NONE,
});


export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case 'SET_IS_TYPING': {
            return state.merge({ isTyping: action.payload.isTyping });
        }
        case 'SET_USERNAME': {
            return state.merge({ username: action.payload.username });
        }
        case 'SEND_MESSAGE_PENDING': {
            return state.merge({ error: null, isTyping: true });
        }
        case 'SEND_MESSAGE_REJECTED': {
            return state.merge({ error: action.payload.error, isTyping: false });
        }
        case 'SEND_MESSAGE_FULFILLED': {
            console.log(action.payload);

            const newMessage = {
                intentName: action.payload.intentName,
                dialogState: action.payload.dialogState,
                messageFormat: action.payload.messageFormat,
                message: action.payload.message,
                timestamp: new Date().valueOf()
            };

            if (newMessage.messageFormat === 'CustomPayload') {
                const messageJson = JSON.parse(newMessage.message);
                newMessage.message = messageJson.message;

                const username = messageJson.attributes.username ? messageJson.attributes.username : state.username;
                const botname = messageJson.attributes.botname ? messageJson.attributes.botname : state.botname;
                return state.merge({ newMessage, username, botname });
            } else {
                return state.merge({ newMessage });
            }
            // return state.merge({ fetching: false, fetched: true, error: null, user });
        }
        case 'CONSUME_NEW_MESSAGE': {
            return state.merge({ newMessage: null });
        }
        default: {
            return state;
        }
    }
}
