import { combineReducers } from 'redux';

import app from './reducer-app';


export default combineReducers({
    app
});
