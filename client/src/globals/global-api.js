import Enum from 'es6-enum';

const API_BASE = 'https://210tmsr1de.execute-api.us-east-1.amazonaws.com/dev/';
const MEDIA_BASE = API_BASE + '/media/';

// if (process.env.NODE_ENV === 'production') {
//     API_BASE = 'http://104.155.168.161';
// }

export default {
    API_BASE,
    MEDIA_BASE
};
