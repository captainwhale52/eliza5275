'use strict';

const AWS = require('aws-sdk');
const lex = new AWS.LexRuntime({apiVersion: process.env.AWS_API_VERSION});

module.exports = function(event) {
    let userId = 'stranger';
    let message = '';
    let contentType = 'text/plain; charset=utf-8';
    let accept = 'text/plain; charset=utf-8';

    console.log(event);

    const requestBody = JSON.parse(event.body);

    if (event.headers['Authorization'] && event.headers['Authorization'] !== 'null') {
        userId = event.headers['Authorization'];
    } else {
        throw new Error(`Authentication is missing.`);
    }

    if (requestBody && requestBody.message) {
        message = requestBody.message;
    }

    console.log(`${userId} => ${message}`);

    return new Promise(function(resolve, reject) {
        lex.postContent({
            botAlias: process.env.LEX_BOT_ALIAS,
            botName: process.env.LEX_BOT_NAME,
            contentType: contentType,
            inputStream: message,
            userId: userId,
            accept: accept
        }, function(error, data) {
            if (error) {
                reject(error);
            } else {
                resolve(data);
            }
        });
    })
        .then(response => {
            console.log(response);
            return {
                statusCode: 200,
                headers: {
                    'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                    // 'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authentication',
                    // 'Access-Control-Allow-Methods': 'GET,POST,OPTIONS'
                },
                body: JSON.stringify(response),
            };
        });
};
