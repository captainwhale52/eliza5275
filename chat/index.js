'use strict';

const dispatch = require('./dispatch');

module.exports.handler = (event, context, callback) => {
    dispatch(event)
        .then(response => callback(null, response))
        .catch(callback);
};