const path = require('path');

const HtmlWebPackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');

const root = path.resolve(__dirname);
const client = path.join(root, './client');
const src = path.join(client, './src');
const dist = path.join(client, './dist');
const favicons = path.join(src, './favicons');
const styles = path.join(src, './styles');

module.exports = (env, argv) => {
    const plugins = [
        new HtmlWebPackPlugin({
            template: path.join(client, './/index.html'),
            filename: "./index.html"
        })
    ];

    if (argv.mode !== 'development') {
        plugins.push(
            new CleanWebpackPlugin(dist),
            new ExtractTextPlugin('[name].[hash].css'),
            new CopyWebpackPlugin([{from: favicons, to: dist}])
        );
    }

    return {
        entry: path.join(src, `./index.js`),
        output: {
            path: dist,
            filename: '[name].[hash].js',
            publicPath: argv.mode === 'production' ? '/' : '/'
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader"
                    }
                },
                {
                    test: /\.html$/,
                    use: [
                        {
                            loader: "html-loader",
                            options: { minimize: true }
                        }
                    ]
                },
                {
                    test: /\.(png|jpg|gif)$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: "[hash].[ext]",
                            }
                        }
                    ]
                },
                {
                    test: /\.txt$/,
                    use: {
                        loader: 'raw-loader',
                        options: {
                            name: "[hash].[ext]"
                        }
                    }
                },
                {
                    test: /\.(css|sass|scss)$/,
                    include: styles,
                    use: argv.mode === 'production' ?
                        ExtractTextPlugin.extract({
                            fallback: 'style-loader',
                            use: [
                                { loader: 'css-loader', query: { modules: true, sourceMaps: false, localIdentName: '[local]' } },
                                { loader: 'postcss-loader' },
                                { loader: 'sass-loader' }
                            ]
                        }) : [
                            'style-loader',
                            'css-loader?modules&localIdentName=[local]',
                            'sass-loader'
                        ]
                },
                {
                    test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                    use: [
                        {
                            loader: "file-loader",
                            options: {
                                name: "[hash].[ext]"
                            },
                        }
                    ]
                }

            ]
        },
        plugins: plugins
    };
};
